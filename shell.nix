{ pkgs ? import <nixpkgs> {} }:

let
  inherit (pkgs) beam;

  erlang = beam.interpreters.erlangR20_nox;
  elixir = (beam.packagesWith erlang).elixir_1_7;

  ruby = pkgs.ruby_2_5;
  rabbitmq-server = pkgs.rabbitmq-server.override { inherit erlang; };
  producer = pkgs.callPackage ./producer { inherit ruby; };
  rabbitmq-enabled-config = builtins.toFile "enabled_plugins" ''
    [rabbitmq_management].
  '';

  procfile = builtins.toFile "Procfile" ''
    broker: rabbitmq-server
    consumer: cd consumer && iex -S mix
  '';
  overmind = pkgs.overmind.overrideAttrs (oldAttrs: rec {
    name = "overmind-${version}";
    version = "2.0.0";

    src = pkgs.fetchFromGitHub {
      owner = "DarthSim";
      repo = "overmind";
      rev = "v${version}";
      sha256 = "1yp45y7bwchyyd296ywvkv50r1s9pa667nqadwsx69y26hrh9624";
    };
  });
in pkgs.mkShell {
  buildInputs = [
    producer
    ruby
    rabbitmq-server
    overmind
    elixir
    erlang
  ];

  shellHook = ''
    mkdir -p $PWD/var

    export RABBITMQ_LOG_BASE=$PWD/var
    export RABBITMQ_MNESIA_BASE=$PWD/var
    export RABBITMQ_ENABLED_PLUGINS_FILE=${rabbitmq-enabled-config}

    export OVERMIND_PROCFILE=${procfile}
    export OVERMIND_SOCKET=$PWD/var/overmind.sock
    export OVERMIND_STOP_SIGNALS=consumer=QUIT
  '';
}
