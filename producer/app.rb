# frozen_string_literal: true

require_relative 'boot'

require 'logger'

class Publisher
  DEFAULT_LOGGER = Logger.new(STDOUT).tap { |l| l.level = Logger::DEBUG }

  def self.open(logger: DEFAULT_LOGGER)
    publisher = new(logger: logger)

    return publisher unless block_given?

    begin
      yield(publisher)
    ensure
      publisher.close
    end
  end

  def initialize(logger: DEFAULT_LOGGER)
    @logger = logger
    @conn = Bunny.new

    setup_exchange
  end

  def publish(topic, message)
    exchange.publish(message, routing_key: topic)
    logger.debug "Sent #{message} to #{topic}"
  end

  def close
    conn.close
  end

  private

  def setup_exchange
    conn.start
    logger.debug "Connected to broker on #{conn.host}:#{conn.port}"

    chan = conn.create_channel
    logger.debug 'Created channel'

    @exchange = chan.topic('logs', durable: true)
    logger.debug "Created #{exchange.name} exchange"
  end

  attr_reader :logger, :conn, :exchange
end

topic = ARGV.shift || 'anonymous.info'
message = ARGV.empty? ? 'Hello World!' : ARGV.join(' ')

Publisher.open { |publisher| publisher.publish(topic, message) }
