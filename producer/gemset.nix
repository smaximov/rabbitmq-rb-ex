{
  amq-protocol = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1kjyphxrlmwd5bmi7q14wv9xklxmm6kq85yxlprqp7x4d16ksmwk";
      type = "gem";
    };
    version = "2.3.0";
  };
  ast = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "184ssy3w93nkajlz2c70ifm79jp3j737294kbc5fjw69v1w0n9x7";
      type = "gem";
    };
    version = "2.4.0";
  };
  binding_of_caller = {
    dependencies = ["debug_inspector"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "05syqlks7463zsy1jdfbbdravdhj9hpj5pv2m74blqpv8bq4vv5g";
      type = "gem";
    };
    version = "0.8.0";
  };
  bunny = {
    dependencies = ["amq-protocol"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1a7qqpiyb3xkbffpbp0hh8r4khmgl3zmciy3bvbpnpp8njnd7g9r";
      type = "gem";
    };
    version = "2.12.1";
  };
  byebug = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1vv7s88w8jb1qg4qz3jrs3x3y5d9jfyyl7wfiz78b5x95ydvx41q";
      type = "gem";
    };
    version = "9.1.0";
  };
  coderay = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "15vav4bhcc2x3jmi3izb11l4d9f3xv8hp2fszb7iqmpsccv1pz4y";
      type = "gem";
    };
    version = "1.1.2";
  };
  debug_inspector = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0vxr0xa1mfbkfcrn71n7c4f2dj7la5hvphn904vh20j3x4j5lrx0";
      type = "gem";
    };
    version = "0.0.3";
  };
  jaro_winkler = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0rr797nqz081bfk30m2apj5h24bg5d1jr1c8p3xwx4hbwsrbclah";
      type = "gem";
    };
    version = "1.5.1";
  };
  method_source = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1pviwzvdqd90gn6y7illcdd9adapw8fczml933p5vl739dkvl3lq";
      type = "gem";
    };
    version = "0.9.2";
  };
  parallel = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "01hj8v1qnyl5ndrs33g8ld8ibk0rbcqdpkpznr04gkbxd11pqn67";
      type = "gem";
    };
    version = "1.12.1";
  };
  parser = {
    dependencies = ["ast"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1zjk0w1kjj3xk8ymy1430aa4gg0k8ckphfj88br6il4pm83f0n1f";
      type = "gem";
    };
    version = "2.5.3.0";
  };
  powerpack = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1r51d67wd467rpdfl6x43y84vwm8f5ql9l9m85ak1s2sp3nc5hyv";
      type = "gem";
    };
    version = "0.1.2";
  };
  pry = {
    dependencies = ["coderay" "method_source"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1mh312k3y94sj0pi160wpia0ps8f4kmzvm505i6bvwynfdh7v30g";
      type = "gem";
    };
    version = "0.11.3";
  };
  pry-byebug = {
    dependencies = ["byebug" "pry"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1f9kj1qp14qb8crg2rdzf22pr6ngxvy4n6ipymla8q1yjr842625";
      type = "gem";
    };
    version = "3.5.1";
  };
  pry-doc = {
    dependencies = ["pry" "yard"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1ar8shi6jl7vsz1f45alfkmgpmb9w5766mz1q27vimckqrdd1ar7";
      type = "gem";
    };
    version = "0.13.5";
  };
  pry-rails = {
    dependencies = ["pry"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1ymygfyk7x8bjba3fw20p4wv0mypmpv6js7bwg2g6icrp1ddzahf";
      type = "gem";
    };
    version = "0.3.8";
  };
  pry-stack_explorer = {
    dependencies = ["binding_of_caller" "pry"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0r95n0p279ycr2bcldbnws5cnkc6m1ylrssxynsp3vxy6cjd7524";
      type = "gem";
    };
    version = "0.4.9.3";
  };
  rainbow = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0bb2fpjspydr6x0s8pn1pqkzmxszvkfapv0p4627mywl7ky4zkhk";
      type = "gem";
    };
    version = "3.0.0";
  };
  rubocop = {
    dependencies = ["jaro_winkler" "parallel" "parser" "powerpack" "rainbow" "ruby-progressbar" "unicode-display_width"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1njsjg60cff085n17qvf2slwhzbxk2lrrfmrvdklhnpfcwak0l46";
      type = "gem";
    };
    version = "0.61.1";
  };
  ruby-progressbar = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1cv2ym3rl09svw8940ny67bav7b2db4ms39i4raaqzkf59jmhglk";
      type = "gem";
    };
    version = "1.10.0";
  };
  unicode-display_width = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0040bsdpcmvp8w31lqi2s9s4p4h031zv52401qidmh25cgyh4a57";
      type = "gem";
    };
    version = "1.4.0";
  };
  yard = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0lmmr1839qgbb3zxfa7jf5mzy17yjl1yirwlgzdhws4452gqhn67";
      type = "gem";
    };
    version = "0.9.16";
  };
}