{ bundlerEnv, ruby }:

bundlerEnv rec {
  name = "producer";
  gemdir = ./.;

  inherit ruby;
}
