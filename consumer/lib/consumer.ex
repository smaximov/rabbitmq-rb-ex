defmodule Consumer do
  @moduledoc false

  alias Consumer.State

  use GenServer

  require Logger

  @exchange "logs"
  @timeout_sec 10
  @timeout :timer.seconds(@timeout_sec)

  def start_link({name, binding_key}) do
    GenServer.start_link(__MODULE__, {name, List.wrap(binding_key)})
  end

  @impl GenServer
  def init(opts), do: rabbitmq_connect(opts)

  defp rabbitmq_connect({name, binding_key}) do
    case AMQP.Connection.open() do
      {:ok, conn} ->
        Logger.info(fn ->
          "#{name} :: connected to broker: #{inspect(conn.pid)}"
        end)

        Process.monitor(conn.pid)
        {:ok, chan} = AMQP.Channel.open(conn)
        :ok = AMQP.Basic.qos(chan, prefetch_count: 10)

        :ok = AMQP.Exchange.topic(chan, @exchange, durable: true)
        {:ok, %{queue: queue}} = AMQP.Queue.declare(chan, "", exclusive: true)

        Logger.debug(fn ->
          "#{name} :: created anonymous queue #{queue}"
        end)

        for routing_key <- binding_key do
          :ok = AMQP.Queue.bind(chan, queue, @exchange, routing_key: routing_key)

          Logger.debug(fn ->
            "#{name} :: bound #{queue} to #{@exchange} using #{routing_key}"
          end)
        end

        {:ok, consumer_tag} = AMQP.Basic.consume(chan, queue)

        {:ok,
         %State{
           conn: conn,
           chan: chan,
           consumer_tag: consumer_tag,
           name: name,
           binding_key: binding_key
         }}

      {:error, reason} ->
        Logger.error(fn ->
          "#{name} :: unable to connect to broker: #{inspect(reason)}, retrying in #{@timeout_sec} seconds"
        end)

        :timer.sleep(@timeout)
        rabbitmq_connect({name, binding_key})
    end
  end

  @impl GenServer
  def handle_info({:DOWN, _, :process, pid, reason}, %State{name: name, binding_key: binding_key}) do
    Logger.error(fn ->
      "#{name} :: disconnected from #{inspect(pid)}, reason: #{inspect(reason)}"
    end)

    {:ok, %State{} = state} = rabbitmq_connect({name, binding_key})
    {:noreply, state}
  end

  def handle_info({:basic_consume_ok, %{consumer_tag: consumer_tag}}, %State{name: name} = state) do
    Logger.debug(fn ->
      "#{name} :: consumer registered: #{inspect(consumer_tag)}"
    end)

    {:noreply, state}
  end

  def handle_info(
        {:basic_deliver, payload, %{delivery_tag: tag, routing_key: routing_key}},
        %State{chan: chan, name: name, consumer_tag: consumer_tag} = state
      ) do
    Logger.debug(fn ->
      "#{name} :: [#{routing_key}] got #{inspect(tag)} from #{inspect(consumer_tag)}: #{payload}"
    end)

    :ok = AMQP.Basic.ack(chan, tag)

    {:noreply, state}
  end
end
