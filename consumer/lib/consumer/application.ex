defmodule Consumer.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children =
      [
        {ConsumeAll, "#"},
        {ConsumeKern, "kern.*"},
        {ConsumeErrorWarn, ~w[*.error *.warn]}
      ]
      |> Enum.map(&worker_spec/1)

    opts = [strategy: :one_for_one, name: Consumer.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def worker_spec({name, binding_key}) do
    Supervisor.child_spec({Consumer, {inspect(name), binding_key}}, id: name)
  end
end
