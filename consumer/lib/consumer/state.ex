defmodule Consumer.State do
  @moduledoc false

  @enforce_keys ~w[name chan consumer_tag conn binding_key]a
  defstruct @enforce_keys
end
